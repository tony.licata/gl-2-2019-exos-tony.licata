/*
** Classe représentant un élément du menu. 
*/
public class MenuItem {
    String name;
    float price;

    MenuItem(String name, float price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return String.format("%s CHF %.2f", this.name, this.price);
    }
}