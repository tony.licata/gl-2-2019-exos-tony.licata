import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Drinks implements Iterable {

    List<MenuItem> items;

    @Override
    public Iterator iterator() {
        return new DrinksIterator(this);
    }

    /*
     * TODO: Complètez la classe DrinksIterator. Si vous estimez qu'il n'y a pas
     * besoins de cette classe, vous êtes libres de faire l'implémentation d'une
     * autre manière.
     */
    private static class DrinksIterator implements Iterator {

        private Drinks drinks;
        private int currentIndex;
        private int drinksSize;

        public DrinksIterator(Drinks drinks) {
            this.drinks = drinks;
            currentIndex = 0;
            drinksSize = drinks.items.size();
        }

        @Override
        public boolean hasNext() {
            return currentIndex<drinksSize;
        }

        @Override
        public Object next() {
            return drinks.items.get(currentIndex++);
        }
    }

    public Drinks() {
        this.items = new ArrayList<>();
        this.items.add(new MenuItem("Bière blonde", 5.0f));
        this.items.add(new MenuItem("Bière blanche", 6.0f));
        this.items.add(new MenuItem("Bière IPA", 6.5f));
        this.items.add(new MenuItem("Pastis", 5.0f));
    }

    /* TODO: ...complétez... */

}