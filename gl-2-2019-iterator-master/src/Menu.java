import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class Menu implements Iterable {

    private final MenuItem[] items = new MenuItem[] { 
        new MenuItem("Fribourger", 13.20f),
        new MenuItem("Andalous", 14),
        new MenuItem("Salade", 10.50f)
    };

    @Override
    public Iterator iterator() {
        return new MenuIterator(this);
    }

    public static class MenuIterator implements Iterator {

        private Menu menu;
        private int currentIndex;
        private int menuSize;

        MenuIterator(Menu m) {
            this.menu = m;
            currentIndex = 0;
            menuSize = menu.items.length;
        }

        @Override
        public boolean hasNext() {
            return currentIndex<menuSize;
        }

        @Override
        public MenuItem next() {
            return menu.items[currentIndex++];
        }

    }

    public Menu() {

    }

    /* TODO: ...complétez... */

}