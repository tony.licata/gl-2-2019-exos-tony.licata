import java.util.Arrays;
import java.util.Iterator;

public class Customers implements Iterable {

    // Tableau contenant les clients du bar qui sont certes assoiffés...
    private final String[] customers = new String[] { "Loïc", "Florent", "Yannis" };

    @Override
    public Iterator iterator() {
        return Arrays.asList(customers).iterator();
    }

    /*
     * TODO: Même exercice que pour la classe Menu mais sans créer une classe
     * Iterator.
     */

}