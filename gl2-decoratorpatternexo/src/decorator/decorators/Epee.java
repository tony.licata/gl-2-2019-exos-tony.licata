package decorator.decorators;

import decorator.DecoracteurPersonnage;
import decorator.Personnage;

public class Epee extends DecoracteurPersonnage {

   public Epee(Personnage p) {
      this.personnage = p;
   }

   @Override
   public String getNom() {
      return personnage.getNom() + " (+épée)";
   }

   @Override
   public int getVie() {
      return personnage.getVie();
   }

   @Override
   public int getAttaque() {
      return personnage.getAttaque() + 20;
   }

   @Override
   public int getDefense() {
      return personnage.getDefense();
   }
}
