package decorator;

public abstract class DecoracteurPersonnage extends Personnage{
   protected Personnage personnage;

   @Override
   public abstract String getNom();

   @Override
   public abstract int getVie();

   @Override
   public abstract int getAttaque();

   @Override
   public abstract int getDefense();
}
