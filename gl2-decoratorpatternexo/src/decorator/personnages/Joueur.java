package decorator.personnages;

import decorator.Personnage;

public class Joueur extends Personnage {
   public Joueur() {
      setNom("Joueur");
      setVie(100);
      setAttaque(10);
      setDefense(10);
   }
}
