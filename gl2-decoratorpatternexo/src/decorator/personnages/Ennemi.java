package decorator.personnages;

import decorator.Personnage;

public class Ennemi extends Personnage {
   public Ennemi() {
      setNom("Ennemi");
      setVie(100);
      setAttaque(10);
      setDefense(10);
   }
}
