package decorator;

public abstract class Personnage {
   private String nom;
   private int vie;
   private int attaque;
   private int defense;

   public String getNom() {
      return nom;
   }

   public void setNom(String nom) {
      this.nom = nom;
   }

   public int getVie() {
      return vie;
   }

   public void setVie(int vie) {
      this.vie = vie;
   }

   public int getAttaque() {
      return attaque;
   }

   public void setAttaque(int attaque) {
      this.attaque = attaque;
   }

   public int getDefense() {
      return defense;
   }

   public void setDefense(int defense) {
      this.defense = defense;
   }

   @Override
   public String toString() {
      return getNom();
   }
}
