import decorator.Personnage;
import decorator.decorators.Bouclier;
import decorator.decorators.Epee;
import decorator.personnages.Ennemi;
import decorator.personnages.Joueur;

public class Main {

   public static void main(String[] args) {
      Personnage joueurSimple = new Joueur();
      System.out.println(joueurSimple);

      Personnage joueurEpeeBouclier = new Joueur();
      joueurEpeeBouclier = new Epee(joueurEpeeBouclier);
      joueurEpeeBouclier = new Bouclier(joueurEpeeBouclier);
      System.out.println(joueurEpeeBouclier);

      Personnage kirito = new Joueur();
      kirito = new Epee(kirito);
      kirito = new Epee(kirito);
      System.out.println(kirito);

      Personnage ennemiBouclier  = new Ennemi();
      ennemiBouclier = new Bouclier(ennemiBouclier);
      System.out.println(ennemiBouclier);
   }
}
