package ex_pattern_observer;

import java.util.Observable;

public class YoutubeChannel extends Observable {
    void news() {
        String[] news = {"New video", "New vlog", "New comment"}; // etc ...
        for(String s: news) {
            // 1) indiquer que l'objet observ� a chang� :
            setChanged();
            // 2) notifier aux observateurs que l'objet observ� a chang� :
            notifyObservers(s);
        }
    }
}