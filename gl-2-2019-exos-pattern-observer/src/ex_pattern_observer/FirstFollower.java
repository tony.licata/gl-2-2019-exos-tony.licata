package ex_pattern_observer;

import java.util.Observable;
import java.util.Observer;

public class FirstFollower implements Observer {

    // 1) ajouter la m�thode n�cessaire � l'impl�mentation :
    // N.B. Pour l'exercice vous pouvez mettre comme corps de la m�thode quelque
    // chose comme : System.out.println("Follower 1 got the news:"+(String)arg);
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Follower 1 got the news : "+(String)arg);
    }
}