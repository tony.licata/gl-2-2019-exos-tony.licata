package ex_pattern_observer;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class AdminObserver implements Observer {
    
    static int nbFollowers = 0;
    static int nbNotifications = 0;
    ArrayList<String> listNotifications = new ArrayList<String>();

    // 1) ajouter la m�thode n�cessaire � l'impl�mentation :
    // N.B. Pour le corps de la m�thode, il faut r�cup�rer le nombre d'observateurs
    // dans "nbFollowers", ajouter � la "listNotifications" la notification si elle
    // n'y est pas d�j� et incr�menter "nbNotifications", afficher ces compteurs avec
    // par exemple :
    // System.out.println("Current number of followers : "+nbFollowers);
    // System.out.println("Current number of notifications : "+nbNotifications);
    @Override
    public void update(Observable o, Object arg) {
        nbFollowers = o.countObservers();
        if(!listNotifications.contains(arg))listNotifications.add((String)arg);
        nbNotifications = listNotifications.size();
        System.out.println("Current number of followers : "+nbFollowers);
        System.out.println("Current number of notifications : "+nbNotifications);
    }

}
