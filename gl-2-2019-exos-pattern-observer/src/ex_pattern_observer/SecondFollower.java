package ex_pattern_observer;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class SecondFollower implements Observer {
    
    static ArrayList<String> notificationsList = new ArrayList<String>();

    // 1) ajouter la m�thode n�cessaire � l'impl�mentation :
    // N.B. Pour le corps de la m�thode, il faut ajouter la nouvelle notification
    // � la "notificationsList" puis afficher tous les �l�ments de la liste.
    @Override
    public void update(Observable o, Object arg) {
        notificationsList.add((String)arg);
        System.out.println("Follower 2 has those notifications :");
        for (String s : notificationsList) {
            System.out.println(s);
        }
    }
    
}