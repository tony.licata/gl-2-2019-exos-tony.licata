package ex_pattern_observer;

class ObserverObservableDemo {
    
    public static void main(String args[]) { 

        // 1) cr�er un objet � observer :
        YoutubeChannel yt = new YoutubeChannel();
        // 2) cr�er un observateurs "admin"
        AdminObserver ao = new AdminObserver();
        // 3) ajouter "admin" � la liste d'observateurs de l'objet :
        yt.addObserver(ao);
        // 4) lancer les notifications (les changements avec les news) :
        yt.news();
        // 5) r�p�ter les actions 2) et 3) pour un "first follower" et un 
        // "second follower"
        FirstFollower ff = new FirstFollower();
        yt.addObserver(ff);
        SecondFollower sf = new SecondFollower();
        yt.addObserver(sf);
        // QUESTION avant le point 6) : Dans quel ordre sont notifi�s les 
        // observateurs selon vous ?
        //Le second, le premier, l'admin
        // 6) relancer les notifications et controler votre r�ponse � la 
        // question qui pr�c�de :
        yt.news();
        //oui
    }
}
