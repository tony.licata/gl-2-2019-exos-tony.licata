public class CarEnglishProxy implements ICar {
    private Driver driver;
    private ICar realCar;

    CarEnglishProxy(Driver driver){
        this.driver = driver;
        realCar = new RealCar();
    }
    
    @Override
    public boolean startCar() {
        if(driver.hasKey())
            return realCar.startCar();
        return false;
    }
    @Override
    public void driveCar() {
        if(driver.getAge()>=18 && driver.hasLicense()){
            System.out.println("The driver is going on the street (at gauche!)");
        }else{
            System.out.println("The driver isn't old enough or hasn't it's license!");
        }
    }
}
