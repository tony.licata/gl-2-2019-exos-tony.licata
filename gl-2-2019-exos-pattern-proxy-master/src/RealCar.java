public class RealCar implements ICar {
    private boolean isStarted = false;
    
    @Override
    public boolean startCar() {
        System.out.println("Car started");
        isStarted = true;
        return isStarted;
    }

    @Override
    public void driveCar() {
        if (!isStarted) {
            System.out.println("Car not started");
            return;
        }
        System.out.println("Driving car");
        isStarted = false;
    }

}
