public class Driver {
    private int age;
    private boolean key;
    private boolean license;
    
    public Driver(int age, boolean key, boolean license) {
        this.age = age;
        this.key = key;
        this.license = license;
    }
    
    public boolean hasKey() {
        return this.key;
    }
    
    public int getAge() {
        return this.age;
    }
    
    public boolean hasLicense() {
        return this.license;
    }
}
