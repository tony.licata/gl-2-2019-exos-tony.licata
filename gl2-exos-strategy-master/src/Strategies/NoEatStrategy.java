package Strategies;

public class NoEatStrategy implements IEatStrategy {
    @Override
    public String eats() {
        return "has no eat";
    }
}
