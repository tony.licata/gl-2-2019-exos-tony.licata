package Strategies;

public class StandardEatStrategy implements IEatStrategy {
    @Override
    public String eats() {
        return "has a standard eat";
    }
}
