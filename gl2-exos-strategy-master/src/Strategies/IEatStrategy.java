package Strategies;

public interface IEatStrategy {
    public String eats();
}
