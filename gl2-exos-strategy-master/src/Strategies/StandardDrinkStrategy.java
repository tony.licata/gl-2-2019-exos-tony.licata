package Strategies;

public class StandardDrinkStrategy implements IDrinkStrategy  {
	public String drinks() {
		return "has a standard drink";
	}
}