package Strategies;

public class AlcoholicDrinkStrategy implements IDrinkStrategy {
	public String drinks() {
		return "has an alcoholic drink";
	}
}