package Strategies;

public class NoDrinkStrategy implements IDrinkStrategy {
	public String drinks() {
		return "has no drink";
	}
}