package Strategies;

public class VegeEatStrategy implements IEatStrategy {
    @Override
    public String eats() {
        return "has a vegan eat";
    }
}
