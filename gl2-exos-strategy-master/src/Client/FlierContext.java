package Client;

import Strategies.IDrinkStrategy;
import Strategies.IEatStrategy;

public class FlierContext {
	//TODO add ohter attribute here
	IEatStrategy iES;
	IDrinkStrategy iDS;

	public FlierContext(Object aIEatStrategy, Object aIDrinkStrategy) { //TODO implement Constructor
		this.iES = (IEatStrategy)aIEatStrategy;
		this.iDS = (IDrinkStrategy)aIDrinkStrategy;
	}

	//TODO implement eat and drink methods
	public String eats(){
		return iES.eats();
	}

	public String drinks(){
		return iDS.drinks();
	}

	//TODO implement setters
	public void setEatStrategy(IEatStrategy iES){
		this.iES = iES;
	}

	public void setDrinkStrategy(IDrinkStrategy iDS){
		this.iDS = iDS;
	}

}