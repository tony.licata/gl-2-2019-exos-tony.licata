package pattern_bridge;

class BridgePatternDemo { 
    public static void main(String[] args) 
    { 
        //creer un vehicule car
        Vehicle car = new Car(new Assembly(),new Production());
        //fabriquer (manufacture) car
        car.manufacture();

        //creer un vehicule bicycle
        Vehicle bicycle = new Bicycle(new Assembly(),new Production());
        //fabriquer (manufacture) bicycle
        bicycle.manufacture();
    } 
} 