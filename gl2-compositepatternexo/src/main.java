
import com.fasterxml.jackson.databind.*;
import java.io.File;
import java.io.IOException;

public class main {

   public static void main(String[] args) {


      ObjectMapper mapper = new ObjectMapper();
      try {
         Employee CEO = mapper.readValue(
                 new File("company.json"),
                 Employee.class);

         Employee marketingHead = CEO.getFullDepartment(Employee.Department.MARKETING);
         System.out.println("Head of marketing department : " + marketingHead.getFirstName()
                 + " " + marketingHead.getLastName());
         System.out.println("Total of marketing salaries : " + marketingHead.getSalarySum());

      }
      catch (IOException e) {
         e.printStackTrace();
      }
   }
}
