import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;


public class Employee {

    public static enum Department {
        EXEC_COMMITTEE,
        MARKETING,
        SALES,
        HR,
        RD
    }

    public static enum Profession {
        CEO,
        CTO,
        CFO,
        HEAD_SALES,
        HEAD_MARKETING,
        HEAD_HR,
        HEAD_RD,
        RESEARCHER,
        RECRUITER,
        SALESMAN,
        MAD_MAN
    }


    /*** ATTRIBUTES ***/
    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("salary")
    private int salary;

    @JsonProperty("department")
    private Department department;

    @JsonProperty("job")
    private Profession job;

    @JsonProperty("subordinates")
    private ArrayList<Employee> subordinates;


    /*** GETTERS ****/
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    /***
     *
     * @return the sum of the salaries of the current employee & all subordinates
     */
    public int getSalarySum() {
       int salarySum = salary;
       if(subordinates!=null) {
           salarySum += subordinates.stream().mapToInt(Employee::getSalarySum).sum();
       }
       return salarySum;
    }

    /***
     *
     * Get the first employee of a given department, i.e. the head of department, i.e. the root node of
     * a whole department
     * @param dept the department to return
     * @return the employee head of the department to return
     */
    public Employee getFullDepartment(Department dept) {
        if(department == dept)return this;
        if(subordinates!=null){
            for (Employee e : subordinates) {
                Employee sub = e.getFullDepartment(dept);
                if(sub!=null)return sub;
            }
        }
        return null;
    }
}
