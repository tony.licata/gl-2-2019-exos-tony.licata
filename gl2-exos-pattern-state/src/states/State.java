package states;

public interface State {
    void action(TrafficLight c);
}
