package states;

public class RedYellow implements State {
    public void action(TrafficLight c) {
        if (c.time >= 2) {
            c.time = 0;
            c.setState(new Green());
            return;
        }
        c.time++;
        System.out.println("Rouge-Jaune");
    }
}
