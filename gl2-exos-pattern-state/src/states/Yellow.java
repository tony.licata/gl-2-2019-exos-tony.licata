package states;

public class Yellow implements State {
    public void action(TrafficLight c) {
        if (c.time >= 3) {
            c.time = 0;
            c.cycles++;
            c.setState(new Red());
            return;
        }
        c.time++;
        System.out.println("Jaune");
    }
}
