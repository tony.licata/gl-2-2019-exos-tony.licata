package states;

public class Red implements State {
    public void action(TrafficLight c) {
        if (c.time >= 10) {
            c.time = 0;
            c.setState(new RedYellow());
            return;
        }
        c.time++;
        System.out.println("Rouge");
    }
}
