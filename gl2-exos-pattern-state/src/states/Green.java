package states;

public class Green implements State {
    public void action(TrafficLight c) {
        if (c.time >= 20) {
            c.time = 0;
            c.setState(new Yellow());
            return;
        }
        c.time++;
        System.out.println("Vert");
    }
}
