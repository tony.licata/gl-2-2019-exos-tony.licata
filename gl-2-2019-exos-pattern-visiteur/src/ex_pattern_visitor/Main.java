package ex_pattern_visitor;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
	
	public static void concert(List<Visitable> animals) {
		// create visitor
		Visitor sound = new Sound();
		// let the visitor visit every animals
		for (Visitable animal : animals) {
			animal.accept(sound);
		}
	}
	
	public static void main(String[] args) {
		List<Visitable> animals = new ArrayList<Visitable>();
		
		Dog dog = new Dog("Bau");
		Cat cat = new Cat("Miao");
		Cow cow = new Cow("Muuu");
		
		// add animals to list
		animals.add(dog);
		animals.add(cat);
		animals.add(cow);

		concert(animals);
	}

}
