package ex_pattern_visitor;


public class Sound implements Visitor{
	@Override
	public void visit(Cat cat) {
		System.out.println(cat.sound);
	}

	@Override
	public void visit(Dog dog) {
		System.out.println(dog.sound);
	}

	@Override
	public void visit(Cow cow) {
		System.out.println(cow.sound);
	}
}
